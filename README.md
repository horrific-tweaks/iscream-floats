This datapack/resourcepack combination adds a craftable "Ice Cream Float" item to the game. It can be crafted with a bucket of powdered snow and a bamboo straw.

![A screenshot of the bamboo crafting recipe in the crafting table](./.images/crafting.png)

Once crafted, the item can be consumed by holding the "use" (right mouse) button. It uses a milk bucket as the base item, so it will remove any other effects. However, there may also be a slightly interesting pun involved.

<details>
  <summary>Spoilers</summary>

  It's an ice cream "float."
</details>

